package com.axonactive.sampleunittesting.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.CallSuper
import com.axonactive.sampleunittesting.di.factory.DaggerViewModelFactory
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

@SuppressLint("Registered")
open class BaseActivity: DaggerAppCompatActivity() {

    private val progressDialog: Dialog by lazy {
        ProgressDialog(this).apply {
            setProgressStyle(ProgressDialog.STYLE_SPINNER)
            setMessage("Please wait")
            isIndeterminate = true
            setCancelable(false)
        }
    }

    @Inject
    lateinit var factory: DaggerViewModelFactory

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observerVMNotification()
    }

    open fun observerVMNotification() {

    }

    fun showLoading() {
        if (isDestroyed) return
        if (!progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    fun hideLoading() {
        if (isDestroyed) return
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun startLaunchIntent() {
        val intent = baseContext.packageManager.getLaunchIntentForPackage(baseContext.packageName)
        intent?.let {
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }
}