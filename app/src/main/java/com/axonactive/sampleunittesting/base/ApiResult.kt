package com.axonactive.sampleunittesting.base

sealed class ApiResult<out R> {
    data class Success<out T>(val data: T) : ApiResult<T>()

    data class Error(val exception: Throwable) : ApiResult<Nothing>()

    object Loading : ApiResult<Nothing>()
}

val ApiResult<*>.succeeded
    get() = this is ApiResult.Success

fun <T> ApiResult<T>.successOr(fallback: T): T {
    val asSuccess = this as? ApiResult.Success<T>
    return if (asSuccess != null) asSuccess.data else fallback
}

fun <T> ApiResult<T>.successOrNull(): T? {
    return (this as? ApiResult.Success<T>)?.data
}

fun <T> ApiResult<T>.failedOrNull(): Throwable? {
    return (this as? ApiResult.Error)?.exception
}
