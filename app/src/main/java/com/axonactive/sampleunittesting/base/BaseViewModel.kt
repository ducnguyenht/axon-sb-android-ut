package com.axonactive.sampleunittesting.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {

    private val disposeOnCleared = CompositeDisposable()

    fun Disposable.untilCleared() = this.also {
        disposeOnCleared.add(this)
    }

    override fun onCleared() {
        super.onCleared()
        disposeOnCleared.clear()
    }

}