package com.axonactive.sampleunittesting.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.axonactive.sampleunittesting.di.ViewModelKey
import com.axonactive.sampleunittesting.di.factory.DaggerViewModelFactory
import com.axonactive.sampleunittesting.feature.login.LoginViewModel
import com.axonactive.sampleunittesting.feature.register.RegisterViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelsModule {

    @Binds
    abstract fun bindFactory(factory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(vm: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    abstract fun bindRegisterViewModel(vm: RegisterViewModel): ViewModel

}