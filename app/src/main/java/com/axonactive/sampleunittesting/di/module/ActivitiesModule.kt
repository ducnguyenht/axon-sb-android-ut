package com.axonactive.sampleunittesting.di.module

import com.axonactive.sampleunittesting.feature.login.LoginActivity
import com.axonactive.sampleunittesting.feature.main.MainActivity
import com.axonactive.sampleunittesting.feature.register.RegisterActivity
import com.axonactive.sampleunittesting.feature.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity
}