package com.axonactive.sampleunittesting.di.module

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.axonactive.sampleunittesting.data.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideAppDataBase(application: Application) : AppDatabase {
        return  Room.databaseBuilder(application, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideApplicationContext(application: Application): Context {
        return application.applicationContext
    }
}