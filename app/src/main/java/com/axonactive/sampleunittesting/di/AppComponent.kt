package com.axonactive.sampleunittesting.di

import android.app.Application
import com.axonactive.sampleunittesting.MainApplication
import com.axonactive.sampleunittesting.di.module.ActivitiesModule
import com.axonactive.sampleunittesting.di.module.AppModule
import com.axonactive.sampleunittesting.di.module.ViewModelsModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Component(modules = [
    AppModule::class,
    AndroidInjectionModule::class,
    ViewModelsModule::class,
    ActivitiesModule::class
])
@Singleton
interface AppComponent : AndroidInjector<MainApplication> {

    override fun inject(application: MainApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}
