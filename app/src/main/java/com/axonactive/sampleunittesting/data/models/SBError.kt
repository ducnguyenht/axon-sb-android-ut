package com.axonactive.sampleunittesting.data.models

class SBError(val code: ErrorCode, message: String) : Throwable(message)