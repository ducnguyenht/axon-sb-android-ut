package com.axonactive.sampleunittesting.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.axonactive.sampleunittesting.data.db.entity.UserEntity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(log: UserEntity): Completable

    @Query("SELECT * FROM UserEntity")
    fun getAll(): Single<List<UserEntity>>

    @Query("DELETE FROM UserEntity WHERE id = :userId")
    fun deleteUser(userId: Int): Completable

    @Query("SELECT * FROM UserEntity WHERE email = :email")
    fun getUserWithEmail(email: String): Single<List<UserEntity>>

    @Query("SELECT * FROM UserEntity WHERE email = :email AND password = :password")
    fun getUserWithAuthInfo(email: String, password: String): Single<List<UserEntity>>
}