package com.axonactive.sampleunittesting.data.models

data class User(val email: String)