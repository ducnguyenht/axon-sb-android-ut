package com.axonactive.sampleunittesting.data.repo

import com.axonactive.sampleunittesting.data.db.AppDatabase
import com.axonactive.sampleunittesting.data.db.entity.UserEntity
import com.axonactive.sampleunittesting.data.db.entity.toUser
import com.axonactive.sampleunittesting.data.models.ErrorCode
import com.axonactive.sampleunittesting.data.models.SBError
import com.axonactive.sampleunittesting.data.models.User
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepo @Inject constructor(private val appDatabase: AppDatabase) {

    fun login(userName: String, password: String): Single<User> {
        return appDatabase.userDao().getUserWithAuthInfo(userName, password)
            .flatMap { entities ->
            if (entities.isNotEmpty()) {
                Single.just(entities.first()).map { it.toUser() }
            } else {
                Single.error(SBError(ErrorCode.USER_NOT_FOUND,"Can't find that user"))
            }
        }
    }

    fun register(email: String, password: String): Completable {
        return appDatabase.userDao().getUserWithEmail(email).flatMapCompletable {
            if (it.isNotEmpty()) {
                Completable.error(SBError(ErrorCode.USER_EXIST,"User with email exists"))
            } else {
                val entity = UserEntity(email = email, password = password)
                appDatabase.userDao().insertUser(entity)
            }
        }
    }
}