package com.axonactive.sampleunittesting.data.repo

import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PrefRepo @Inject constructor(appContext: Context) {

    private val sharedPref = appContext.getSharedPreferences(SHARED_PREF_FILE_NAME, 0)

    var isLoggedIn: Boolean
        get() {
            return sharedPref.getBoolean(KEY_LOGGED_IN, false)
        }
        set(value) {
            sharedPref.edit().putBoolean(KEY_LOGGED_IN, value).apply()
        }

    fun clear() {
        sharedPref.edit().clear().apply()
    }

    companion object {
        const val SHARED_PREF_FILE_NAME = "SimpleAuth"
        const val KEY_LOGGED_IN = "logged_in"
    }
}