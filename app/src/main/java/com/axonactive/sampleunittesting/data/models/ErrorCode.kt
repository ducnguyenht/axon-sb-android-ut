package com.axonactive.sampleunittesting.data.models

enum class ErrorCode {
    USERNAME_EMPTY,
    USERNAME_INVALID,
    PASSWORD_EMPTY,
    PASSWORD_INVALID,
    USER_NOT_FOUND,
    USER_EXIST
}