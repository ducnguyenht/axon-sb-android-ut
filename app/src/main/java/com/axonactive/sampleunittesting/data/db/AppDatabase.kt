package com.axonactive.sampleunittesting.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.axonactive.sampleunittesting.data.db.dao.UserDao
import com.axonactive.sampleunittesting.data.db.entity.UserEntity

@Database(entities = [UserEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {
        const val DB_NAME = "SimpleAuth"
    }
}
