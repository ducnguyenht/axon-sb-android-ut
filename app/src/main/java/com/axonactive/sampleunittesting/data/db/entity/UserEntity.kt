package com.axonactive.sampleunittesting.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.axonactive.sampleunittesting.data.models.User

@Entity
data class UserEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val email: String,
    val password: String
)

fun UserEntity.toUser(): User {
    return User(email)
}