package com.axonactive.sampleunittesting.extensions


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

inline fun <reified VM : ViewModel> FragmentActivity.viewModelProvider(crossinline component: () -> ViewModelProvider.Factory) = lazy {
    ViewModelProviders.of(this, component.invoke())[VM::class.java]
}

inline fun <reified VM : ViewModel> Fragment.viewModelProvider(crossinline component: () -> ViewModelProvider.Factory) = lazy {
    ViewModelProviders.of(this, component.invoke())[VM::class.java]
}

inline fun <reified VM : ViewModel> FragmentActivity.defaultProvider() = lazy {
    ViewModelProviders.of(this)[VM::class.java]
}

inline fun <reified VM : ViewModel> Fragment.defaultProvider() = lazy {
    ViewModelProviders.of(this)[VM::class.java]
}
