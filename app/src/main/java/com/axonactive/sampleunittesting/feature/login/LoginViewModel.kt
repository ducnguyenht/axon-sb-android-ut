package com.axonactive.sampleunittesting.feature.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.axonactive.sampleunittesting.base.ApiResult
import com.axonactive.sampleunittesting.base.BaseViewModel
import com.axonactive.sampleunittesting.data.models.ErrorCode
import com.axonactive.sampleunittesting.data.models.SBError
import com.axonactive.sampleunittesting.data.models.User
import com.axonactive.sampleunittesting.data.repo.PrefRepo
import com.axonactive.sampleunittesting.data.repo.UserRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val userRepo: UserRepo,
    private val prefRepo: PrefRepo
): BaseViewModel() {

    private val loginData = MutableLiveData<ApiResult<User>>()
    val exposedLoginData : LiveData<ApiResult<User>> = loginData

    fun login(email: String, password: String) {
        validate(email, password)?.let {
            loginData.postValue(it)
            return
        }

        userRepo.login(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { loginData.postValue(ApiResult.Loading) }
            .subscribe(
                {
                    loginData.postValue(ApiResult.Success(it))
                    prefRepo.isLoggedIn = true
                },
                {
                    loginData.postValue(ApiResult.Error(it))
                }
            )
            .untilCleared()
    }

    private fun validate(email: String, password: String): ApiResult.Error? {
        if (email.isBlank()) return ApiResult.Error(SBError(ErrorCode.USERNAME_EMPTY, "Empty username"))
        if (password.isBlank()) return ApiResult.Error(SBError(ErrorCode.PASSWORD_EMPTY, "Empty password"))
        return null
    }
}