package com.axonactive.sampleunittesting.feature.main

import android.os.Bundle
import com.axonactive.sampleunittesting.R
import com.axonactive.sampleunittesting.base.BaseActivity
import com.axonactive.sampleunittesting.data.repo.PrefRepo
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity: BaseActivity() {

    @Inject
    lateinit var prefRepo: PrefRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_logout.setOnClickListener {
            prefRepo.clear()
            startLaunchIntent()
        }
    }
}