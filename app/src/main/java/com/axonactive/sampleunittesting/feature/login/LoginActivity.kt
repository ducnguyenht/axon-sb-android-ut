package com.axonactive.sampleunittesting.feature.login

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.axonactive.sampleunittesting.R
import com.axonactive.sampleunittesting.base.ApiResult
import com.axonactive.sampleunittesting.base.BaseActivity
import com.axonactive.sampleunittesting.extensions.viewModelProvider
import com.axonactive.sampleunittesting.feature.main.MainActivity
import com.axonactive.sampleunittesting.feature.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    private val loginVM: LoginViewModel by viewModelProvider { factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        tv_action_register.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }

        btn_submit.setOnClickListener {
            val email = edt_email.text.toString()
            val password = edt_password.text.toString()
            loginVM.login(email, password)
        }
    }

    override fun observerVMNotification() {
        super.observerVMNotification()
        loginVM.exposedLoginData.observe(this, Observer { result ->
            when (result) {
                is ApiResult.Loading -> showLoading()
                is ApiResult.Success -> {
                    hideLoading()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                is ApiResult.Error -> {
                    hideLoading()
                    showToast(result.exception.localizedMessage ?: "unknown error")
                }
            }
        })
    }
}
