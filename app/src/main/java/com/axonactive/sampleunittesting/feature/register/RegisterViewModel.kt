package com.axonactive.sampleunittesting.feature.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.axonactive.sampleunittesting.base.ApiResult
import com.axonactive.sampleunittesting.base.BaseViewModel
import com.axonactive.sampleunittesting.data.models.ErrorCode
import com.axonactive.sampleunittesting.data.models.SBError
import com.axonactive.sampleunittesting.data.repo.UserRepo
import com.axonactive.sampleunittesting.utils.Validator.validateEmail
import com.axonactive.sampleunittesting.utils.Validator.validatePassword
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
   private val userRepo: UserRepo
): BaseViewModel() {

    private val registerData = MutableLiveData<ApiResult<Unit>>()
    val exposedRegisterData : LiveData<ApiResult<Unit>> = registerData

    fun register(email: String, password: String) {
        validate(email, password)?.let {
            registerData.postValue(it)
            return
        }

        userRepo.register(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { registerData.postValue(ApiResult.Loading) }
            .subscribe(
                {
                    registerData.postValue(ApiResult.Success(Unit))
                },
                {
                    registerData.postValue(ApiResult.Error(it))
                }
            )
            .untilCleared()
    }

    private fun validate(email: String, password: String): ApiResult.Error? {
        if (!validateEmail(email)) return ApiResult.Error(SBError(ErrorCode.USERNAME_INVALID, "Invalid username"))
        if (!validatePassword(password)) return ApiResult.Error(SBError(ErrorCode.PASSWORD_INVALID, "Invalid Password"))
        return null
    }
}