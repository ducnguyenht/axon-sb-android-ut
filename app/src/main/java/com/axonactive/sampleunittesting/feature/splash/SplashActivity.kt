package com.axonactive.sampleunittesting.feature.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import com.axonactive.sampleunittesting.R
import com.axonactive.sampleunittesting.base.BaseActivity
import com.axonactive.sampleunittesting.data.repo.PrefRepo
import com.axonactive.sampleunittesting.feature.login.LoginActivity
import com.axonactive.sampleunittesting.feature.main.MainActivity
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashActivity : BaseActivity() {

    @Inject
    lateinit var prefRepo: PrefRepo

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Observable.just(true)
            .delay(1, TimeUnit.SECONDS)
            .subscribe {
                val clazz =  if (!prefRepo.isLoggedIn) {
                    LoginActivity::class.java
                } else {
                    MainActivity::class.java
                }
                val intent = Intent(this, clazz)
                startActivity(intent)
                finish()
            }
    }
}