package com.axonactive.sampleunittesting.feature.register

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.axonactive.sampleunittesting.R
import com.axonactive.sampleunittesting.base.ApiResult
import com.axonactive.sampleunittesting.base.BaseActivity
import com.axonactive.sampleunittesting.extensions.viewModelProvider
import com.axonactive.sampleunittesting.feature.login.LoginActivity
import com.axonactive.sampleunittesting.feature.login.LoginViewModel
import com.axonactive.sampleunittesting.feature.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.btn_submit
import kotlinx.android.synthetic.main.activity_register.edt_email
import kotlinx.android.synthetic.main.activity_register.edt_password

class RegisterActivity : BaseActivity() {

    private val registerVM : RegisterViewModel by viewModelProvider { factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        tv_action_login.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        btn_submit.setOnClickListener {
            val email = edt_email.text.toString()
            val password = edt_password.text.toString()
            registerVM.register(email, password)
        }
    }

    override fun observerVMNotification() {
        super.observerVMNotification()
        registerVM.exposedRegisterData.observe(this, Observer { result ->
            when (result) {
                is ApiResult.Loading -> showLoading()
                is ApiResult.Success -> {
                    hideLoading()
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                is ApiResult.Error -> {
                    hideLoading()
                    showToast(result.exception.localizedMessage)
                }
            }
        })
    }
}