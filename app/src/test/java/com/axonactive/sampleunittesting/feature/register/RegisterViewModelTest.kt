package com.axonactive.sampleunittesting.feature.register

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.axonactive.sampleunittesting.base.ApiResult
import com.axonactive.sampleunittesting.data.models.ErrorCode
import com.axonactive.sampleunittesting.data.models.SBError
import com.axonactive.sampleunittesting.data.repo.UserRepo
import com.axonactive.sampleunittesting.rule.RxImmediateSchedulerRule
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Completable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test

class RegisterViewModelTest {
    private val userRepo = mockk<UserRepo>()
    private val model = RegisterViewModel(userRepo)

    @Rule
    @JvmField
    val schedulersRule = RxImmediateSchedulerRule()

    @Rule
    @JvmField
    val liveDataRule = InstantTaskExecutorRule()

    @Test
    fun `register should throw USERNAME_INVALID when username is invalid`() {
    }

    @Test
    fun `register should throw PASSWORD_INVALID when password invalid`() {
    }

    @Test
    fun `register should throw correct error when user input wrong account`() {
    }

    @Test
    fun `register success`() {
    }
}