package com.axonactive.sampleunittesting.feature.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.axonactive.sampleunittesting.base.ApiResult
import com.axonactive.sampleunittesting.data.models.ErrorCode
import com.axonactive.sampleunittesting.data.models.SBError
import com.axonactive.sampleunittesting.data.models.User
import com.axonactive.sampleunittesting.data.repo.PrefRepo
import com.axonactive.sampleunittesting.data.repo.UserRepo
import com.axonactive.sampleunittesting.rule.RxImmediateSchedulerRule
import io.mockk.*
import io.reactivex.Single
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class LoginViewModelTest {
    private val userRepo = mockk<UserRepo>()
    private val prefRepo = mockk<PrefRepo>()
    private val model = LoginViewModel(userRepo, prefRepo)

    @Rule
    @JvmField
    val schedulersRule = RxImmediateSchedulerRule()

    @Rule
    @JvmField
    val liveDataRule = InstantTaskExecutorRule()

    @Test
    fun `login should throw USERNAME_EMPTY when username is blank`() {
    }

    @Test
    fun `login should throw PASSWORD_EMPTY when password is blank`() {
    }

    @Test
    fun `login should throw correct error when user input wrong account`() {
    }

    @Test
    fun `login should return user info and save to pref when user login success`() {
    }
}