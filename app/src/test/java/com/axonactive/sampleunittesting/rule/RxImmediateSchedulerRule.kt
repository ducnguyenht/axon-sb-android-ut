package com.axonactive.sampleunittesting.rule


import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class RxImmediateSchedulerRule : TestWatcher() {

    private val immediateSchedulers = Schedulers.trampoline()

    override fun starting(description: Description?) {
        super.starting(description)
        RxJavaPlugins.setInitIoSchedulerHandler { immediateSchedulers }
        RxJavaPlugins.setInitComputationSchedulerHandler { immediateSchedulers }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { immediateSchedulers }
        RxJavaPlugins.setInitSingleSchedulerHandler { immediateSchedulers }
        RxJavaPlugins.setIoSchedulerHandler { immediateSchedulers }
        RxJavaPlugins.setComputationSchedulerHandler { immediateSchedulers }
        RxJavaPlugins.setNewThreadSchedulerHandler { immediateSchedulers }
        RxJavaPlugins.setSingleSchedulerHandler { immediateSchedulers }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediateSchedulers }
        RxAndroidPlugins.setMainThreadSchedulerHandler { immediateSchedulers }
    }

    override fun finished(description: Description?) {
        super.finished(description)
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}
