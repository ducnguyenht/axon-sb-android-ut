package com.axonactive.sampleunittesting.data.repo

import com.axonactive.sampleunittesting.data.db.AppDatabase
import com.axonactive.sampleunittesting.data.db.dao.UserDao
import com.axonactive.sampleunittesting.data.db.entity.UserEntity
import com.axonactive.sampleunittesting.data.models.ErrorCode
import com.axonactive.sampleunittesting.data.models.SBError
import com.axonactive.sampleunittesting.rule.RxImmediateSchedulerRule
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule

class UserRepoTest {
    @Rule
    @JvmField
    val schedulersRule = RxImmediateSchedulerRule()

    private val userDao = mockk<UserDao>()
    private val appDatabase = mockk<AppDatabase> {
        every { userDao() } returns userDao
    }
    private val userRepo = UserRepo(appDatabase)

    @Test
    fun `login should return user with email info when success`() {
    }

    @Test
    fun `login should return error when get user error`() {
    }

    @Test
    fun `login should return error when not found user in db`() {
    }

    @Test
    fun `register fail should return error when get user error`() {
    }

    @Test
    fun `register fail should return error when user is exist`() {
    }

    @Test
    fun `register success should return success`() {
    }
}